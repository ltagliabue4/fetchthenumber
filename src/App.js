import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomePage from './HomePage'
import Exercise from './Exercise'

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route
            exact
            path="/"
            component={HomePage}
          />
          <Route
            path="/exercise"
            component={Exercise}
          />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
