const TITLE = "Radicalbit job interview";
const SUBTITLE = "Junior frontend developer";
const DESC = "The aim of the exercise is to play a little bit with promises and react state. The goal is to implement a component with a button: clicking on that button starts the logic to retreive an integer from a fake REST API. Based on the value of the integer the component has to change something into itself. If the integer N is even, show your name N times on screen. If the integer N is odd, the number must be printed nicely on the screen in a container with a green background. If the integer N is greater than 21 disable the button. Since we don't want to build a real REST API we can simulate that using promises instead of calling a real endpoint with the fetch method."


const constants = {
    TITLE,
    SUBTITLE,
    DESC
};

export default constants;