import React from 'react';
import { useHistory } from 'react-router';
import constants from './constants'

const { TITLE, SUBTITLE, DESC } = constants
const HomePage = () => {
    const history = useHistory()
    const handleOnClick = () => {
        history.push('exercise')
    }

    return (
        <>
            <h1>{TITLE}</h1>
            <h3>{SUBTITLE}</h3>
            <div className="exercise-description">{DESC}</div>
            <button onClick={handleOnClick}>Start the exercise</button>
        </>
    )
}

export default React.memo(HomePage)