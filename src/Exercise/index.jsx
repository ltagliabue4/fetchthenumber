import React from 'react';
import { useHistory } from 'react-router';

const Exercise = () => {
    const history = useHistory()
    const handleOnClick = () => history.push("/")
    
    return (
        <>
            <div>Good Luck</div>
            <button onClick={handleOnClick}>Home Page</button>
        </>
    );
}

export default React.memo(Exercise)